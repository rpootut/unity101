﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void GUI_Demo::Start()
extern void GUI_Demo_Start_m6753CC1A6D2044AF9C77B4C7273F94304B68E462 (void);
// 0x00000002 System.Void GUI_Demo::OnGUI()
extern void GUI_Demo_OnGUI_mEB66A73B367C1BCF6D9A1BAEC0561603C34141B7 (void);
// 0x00000003 System.Void GUI_Demo::DoMyWindow(System.Int32)
extern void GUI_Demo_DoMyWindow_m45D8A48E8733E60FB231419B8874EC28E0F3A1E7 (void);
// 0x00000004 System.Void GUI_Demo::.ctor()
extern void GUI_Demo__ctor_m19ED6E96FF715751F68BEE7444656308611A14A3 (void);
// 0x00000005 System.Void AvanzarComportamiento::Update()
extern void AvanzarComportamiento_Update_mBF1215A84F52D149369606CFD05248CC4F5797BB (void);
// 0x00000006 System.Void AvanzarComportamiento::.ctor()
extern void AvanzarComportamiento__ctor_mE5976B988A8CE705D7921A7D523CEE2D44E2313D (void);
// 0x00000007 System.Void BarraVida::Start()
extern void BarraVida_Start_mE9526E35DDEB16B4A94217BC5BDF3A9E39BE15F8 (void);
// 0x00000008 System.Void BarraVida::Update()
extern void BarraVida_Update_mCCCEC28FD0D4CB03F9248814982B40357A53C785 (void);
// 0x00000009 System.Void BarraVida::.ctor()
extern void BarraVida__ctor_m478A30B63EDB7F96E8B3731A1AEFB7A8DB7D8AB8 (void);
// 0x0000000A System.Void DispararComportamiento::Update()
extern void DispararComportamiento_Update_m35C3F59DDCC3CFB8EB8396A4F4BCE10A0607475B (void);
// 0x0000000B System.Void DispararComportamiento::.ctor()
extern void DispararComportamiento__ctor_mA1C1EB33871217C75E721B1E884E86972246CE28 (void);
// 0x0000000C System.Void EventoBala::OnTriggerEnter(UnityEngine.Collider)
extern void EventoBala_OnTriggerEnter_mE559463658D9965E8810301C380E448C9E026D4E (void);
// 0x0000000D System.Void EventoBala::.ctor()
extern void EventoBala__ctor_m43D6BC07CACC6D0A55C7A623B326D691B0BFB5E1 (void);
// 0x0000000E System.Void MovimientoController::Update()
extern void MovimientoController_Update_m1A23394AC8545BF812E88E683FAED39F10C60C82 (void);
// 0x0000000F System.Void MovimientoController::.ctor()
extern void MovimientoController__ctor_m9F1DDED87E0DF777233082245A735EA7C0146A98 (void);
// 0x00000010 System.Void Pausa::Start()
extern void Pausa_Start_m379CF2CA9E828F699DE9FAE6611555A7FF6B7622 (void);
// 0x00000011 System.Void Pausa::Update()
extern void Pausa_Update_mF8A56C9C51C5EB14A5B99AC1A6A1A9BFA94CCF02 (void);
// 0x00000012 System.Void Pausa::Resume()
extern void Pausa_Resume_mB6992314C332FB6193213118D6923FB052058559 (void);
// 0x00000013 System.Void Pausa::.ctor()
extern void Pausa__ctor_m3025D5274AD844EF4940CDD58A010F128CC3221C (void);
// 0x00000014 System.Void PlayerMovement::Start()
extern void PlayerMovement_Start_mB585552228B1908E44D3A69496598FB485F608B6 (void);
// 0x00000015 System.Void PlayerMovement::Update()
extern void PlayerMovement_Update_mC3491BD6CDFF1FA543B16969144C939B2298052F (void);
// 0x00000016 System.Void PlayerMovement::.ctor()
extern void PlayerMovement__ctor_mBF9F632DD9929DD6FF092A968649A4406BFE397F (void);
// 0x00000017 System.Void Vida::Start()
extern void Vida_Start_mC68F7B8FDD945434F957D43FAE8BFCE5604ED5B5 (void);
// 0x00000018 System.Void Vida::Update()
extern void Vida_Update_m005476FD340EA5FBC4FCBE634840107FEE383B6D (void);
// 0x00000019 System.Void Vida::.ctor()
extern void Vida__ctor_m510046C9FAF7857DB658C85226601374DE81FF92 (void);
// 0x0000001A System.Void Readme::.ctor()
extern void Readme__ctor_mF465410C5B2E598F2685E82CFCE1F42186AFF448 (void);
// 0x0000001B System.Void Readme/Section::.ctor()
extern void Section__ctor_mBAD5262A353BC071C61B8DB462A3D4D5AB5C7C4E (void);
// 0x0000001C System.Void UnityTemplateProjects.SimpleCameraController::OnEnable()
extern void SimpleCameraController_OnEnable_m2BA1F31BEDE84695933E86CF731059D6FAC2111C (void);
// 0x0000001D UnityEngine.Vector3 UnityTemplateProjects.SimpleCameraController::GetInputTranslationDirection()
extern void SimpleCameraController_GetInputTranslationDirection_m924AB4CEA66ADC4F1C63EAC21B660E0495765F4A (void);
// 0x0000001E System.Void UnityTemplateProjects.SimpleCameraController::Update()
extern void SimpleCameraController_Update_m0E587F33074BE72C54BA835B2217B0E8EC94F24A (void);
// 0x0000001F System.Single UnityTemplateProjects.SimpleCameraController::GetBoostFactor()
extern void SimpleCameraController_GetBoostFactor_m42B667ADEACC6F9CF0E4B6549A501609A15716AE (void);
// 0x00000020 UnityEngine.Vector2 UnityTemplateProjects.SimpleCameraController::GetInputLookRotation()
extern void SimpleCameraController_GetInputLookRotation_m4F945850C9036FAF5C3436AA42A1C3CB918E1BB9 (void);
// 0x00000021 System.Boolean UnityTemplateProjects.SimpleCameraController::IsBoostPressed()
extern void SimpleCameraController_IsBoostPressed_m26120B18155EBB68597EA52F392AD1E59F9A71E0 (void);
// 0x00000022 System.Boolean UnityTemplateProjects.SimpleCameraController::IsEscapePressed()
extern void SimpleCameraController_IsEscapePressed_mC694179281D2F4B20E831995751C70A26806488D (void);
// 0x00000023 System.Boolean UnityTemplateProjects.SimpleCameraController::IsCameraRotationAllowed()
extern void SimpleCameraController_IsCameraRotationAllowed_mABB9E26C1CC5C5643B305685F046B4B8493C7AFA (void);
// 0x00000024 System.Boolean UnityTemplateProjects.SimpleCameraController::IsRightMouseButtonDown()
extern void SimpleCameraController_IsRightMouseButtonDown_m0DE4F75C0A0AE963F333712F226000E0AE2DC32A (void);
// 0x00000025 System.Boolean UnityTemplateProjects.SimpleCameraController::IsRightMouseButtonUp()
extern void SimpleCameraController_IsRightMouseButtonUp_mEDE93C76DE7120043B15F390865340D1C5165CE5 (void);
// 0x00000026 System.Void UnityTemplateProjects.SimpleCameraController::.ctor()
extern void SimpleCameraController__ctor_mC6AE2509DDF461856450EC7DE0058A1687AB5C87 (void);
// 0x00000027 System.Void UnityTemplateProjects.SimpleCameraController/CameraState::SetFromTransform(UnityEngine.Transform)
extern void CameraState_SetFromTransform_mAF13C515CFB1085295C01A870D93375E98F16647 (void);
// 0x00000028 System.Void UnityTemplateProjects.SimpleCameraController/CameraState::Translate(UnityEngine.Vector3)
extern void CameraState_Translate_mB8F7239BD9DB70190E59D47D75DD125AD9AF3A96 (void);
// 0x00000029 System.Void UnityTemplateProjects.SimpleCameraController/CameraState::LerpTowards(UnityTemplateProjects.SimpleCameraController/CameraState,System.Single,System.Single)
extern void CameraState_LerpTowards_mF2D4B962A677B281ED2F539A2FFF8A693FB9A326 (void);
// 0x0000002A System.Void UnityTemplateProjects.SimpleCameraController/CameraState::UpdateTransform(UnityEngine.Transform)
extern void CameraState_UpdateTransform_mE653356FD34828D19ECB6793439A14C38F372410 (void);
// 0x0000002B System.Void UnityTemplateProjects.SimpleCameraController/CameraState::.ctor()
extern void CameraState__ctor_m9C5338CABE70B8C73F8A4A08C1AFA1B33417DE9D (void);
static Il2CppMethodPointer s_methodPointers[43] = 
{
	GUI_Demo_Start_m6753CC1A6D2044AF9C77B4C7273F94304B68E462,
	GUI_Demo_OnGUI_mEB66A73B367C1BCF6D9A1BAEC0561603C34141B7,
	GUI_Demo_DoMyWindow_m45D8A48E8733E60FB231419B8874EC28E0F3A1E7,
	GUI_Demo__ctor_m19ED6E96FF715751F68BEE7444656308611A14A3,
	AvanzarComportamiento_Update_mBF1215A84F52D149369606CFD05248CC4F5797BB,
	AvanzarComportamiento__ctor_mE5976B988A8CE705D7921A7D523CEE2D44E2313D,
	BarraVida_Start_mE9526E35DDEB16B4A94217BC5BDF3A9E39BE15F8,
	BarraVida_Update_mCCCEC28FD0D4CB03F9248814982B40357A53C785,
	BarraVida__ctor_m478A30B63EDB7F96E8B3731A1AEFB7A8DB7D8AB8,
	DispararComportamiento_Update_m35C3F59DDCC3CFB8EB8396A4F4BCE10A0607475B,
	DispararComportamiento__ctor_mA1C1EB33871217C75E721B1E884E86972246CE28,
	EventoBala_OnTriggerEnter_mE559463658D9965E8810301C380E448C9E026D4E,
	EventoBala__ctor_m43D6BC07CACC6D0A55C7A623B326D691B0BFB5E1,
	MovimientoController_Update_m1A23394AC8545BF812E88E683FAED39F10C60C82,
	MovimientoController__ctor_m9F1DDED87E0DF777233082245A735EA7C0146A98,
	Pausa_Start_m379CF2CA9E828F699DE9FAE6611555A7FF6B7622,
	Pausa_Update_mF8A56C9C51C5EB14A5B99AC1A6A1A9BFA94CCF02,
	Pausa_Resume_mB6992314C332FB6193213118D6923FB052058559,
	Pausa__ctor_m3025D5274AD844EF4940CDD58A010F128CC3221C,
	PlayerMovement_Start_mB585552228B1908E44D3A69496598FB485F608B6,
	PlayerMovement_Update_mC3491BD6CDFF1FA543B16969144C939B2298052F,
	PlayerMovement__ctor_mBF9F632DD9929DD6FF092A968649A4406BFE397F,
	Vida_Start_mC68F7B8FDD945434F957D43FAE8BFCE5604ED5B5,
	Vida_Update_m005476FD340EA5FBC4FCBE634840107FEE383B6D,
	Vida__ctor_m510046C9FAF7857DB658C85226601374DE81FF92,
	Readme__ctor_mF465410C5B2E598F2685E82CFCE1F42186AFF448,
	Section__ctor_mBAD5262A353BC071C61B8DB462A3D4D5AB5C7C4E,
	SimpleCameraController_OnEnable_m2BA1F31BEDE84695933E86CF731059D6FAC2111C,
	SimpleCameraController_GetInputTranslationDirection_m924AB4CEA66ADC4F1C63EAC21B660E0495765F4A,
	SimpleCameraController_Update_m0E587F33074BE72C54BA835B2217B0E8EC94F24A,
	SimpleCameraController_GetBoostFactor_m42B667ADEACC6F9CF0E4B6549A501609A15716AE,
	SimpleCameraController_GetInputLookRotation_m4F945850C9036FAF5C3436AA42A1C3CB918E1BB9,
	SimpleCameraController_IsBoostPressed_m26120B18155EBB68597EA52F392AD1E59F9A71E0,
	SimpleCameraController_IsEscapePressed_mC694179281D2F4B20E831995751C70A26806488D,
	SimpleCameraController_IsCameraRotationAllowed_mABB9E26C1CC5C5643B305685F046B4B8493C7AFA,
	SimpleCameraController_IsRightMouseButtonDown_m0DE4F75C0A0AE963F333712F226000E0AE2DC32A,
	SimpleCameraController_IsRightMouseButtonUp_mEDE93C76DE7120043B15F390865340D1C5165CE5,
	SimpleCameraController__ctor_mC6AE2509DDF461856450EC7DE0058A1687AB5C87,
	CameraState_SetFromTransform_mAF13C515CFB1085295C01A870D93375E98F16647,
	CameraState_Translate_mB8F7239BD9DB70190E59D47D75DD125AD9AF3A96,
	CameraState_LerpTowards_mF2D4B962A677B281ED2F539A2FFF8A693FB9A326,
	CameraState_UpdateTransform_mE653356FD34828D19ECB6793439A14C38F372410,
	CameraState__ctor_m9C5338CABE70B8C73F8A4A08C1AFA1B33417DE9D,
};
static const int32_t s_InvokerIndices[43] = 
{
	3457,
	3457,
	2757,
	3457,
	3457,
	3457,
	3457,
	3457,
	3457,
	3457,
	3457,
	2779,
	3457,
	3457,
	3457,
	3457,
	3457,
	3457,
	3457,
	3457,
	3457,
	3457,
	3457,
	3457,
	3457,
	3457,
	3457,
	3457,
	3450,
	3457,
	3419,
	3448,
	3411,
	3411,
	3411,
	3411,
	3411,
	3457,
	2779,
	2851,
	851,
	2779,
	3457,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	43,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
