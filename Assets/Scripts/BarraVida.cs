using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraVida : MonoBehaviour
{

    private Image _barra;

    [SerializeField]
    private Vida vida;

    // Start is called before the first frame update
    void Start()
    {
        _barra = GetComponent<Image>();
    }
    
    // Update is called once per frame
    void Update()
    {
        _barra.fillAmount = vida.value / 100;
    }
}
