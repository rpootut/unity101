using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DispararComportamiento : MonoBehaviour
{
    [SerializeField]
    private GameObject prefab;//aqui esta la bala

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            GameObject bala = Instantiate(prefab);
            bala.transform.position = transform.position;
            Destroy(bala, 3);
        }
    }
}
