using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoController : MonoBehaviour
{

    [SerializeField]
    [Range(1, 100)]
    private float speed = 1;

    // Update is called once per frame
    void Update()
    {

        float space = speed * Time.deltaTime;

        if (Input.GetKey(KeyCode.Q))
        {

            transform.Translate(0, space, 0);
        }
        if (Input.GetKey(KeyCode.E))
        {

            transform.Translate(0, -space, 0);
        }
    }
}
