using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvanzarComportamiento : MonoBehaviour
{
    [SerializeField]
    [Range(1,400)]
    private float speed;

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0, 0, speed * Time.deltaTime);
    }
}
